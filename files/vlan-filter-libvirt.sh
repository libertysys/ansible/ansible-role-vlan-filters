#!/bin/bash

# Copyright (c) 2022, Paul D. Gear
# License: AGPL v3 or later

# External dependencies:
# - jq
# - logger (bsdutils)
# - xpath (libxml-xpath-perl)

set -e
set -u

BASEPROG=vlan-filter-libvirt
CONF=/etc/vlan-filters.json

configure_vlans()
{
    NAME="$1"
    IFACE="$2"

    TAGGED=$(jq -r ".guests.$NAME.tagged[]" < ${CONF} 2>/dev/null | grep -ve "^null$" || true)
    UNTAGGED=$(jq -r ".guests.$NAME.untagged" < ${CONF} 2>/dev/null | grep -ve "^null"$ || true)

    echo NAME=$NAME
    echo IFACE=$IFACE
    echo TAGGED=$TAGGED
    echo UNTAGGED=$UNTAGGED

    # clear default VLAN
    bridge vlan del dev $IFACE vid 1

    # add untagged VLAN
    if [ -n "$UNTAGGED" ]; then
        bridge vlan add dev $IFACE vid $UNTAGGED pvid untagged
    fi

    # add tagged VLANs
    for vlan in $TAGGED; do
        bridge vlan add dev $IFACE vid $vlan
    done

    # show results
    bridge vlan list
}

(
NAME="$1"
ACTION="$2"
#BEGIN_END="$3"
#TERMINATOR="$4"

case $ACTION in

    prepare)
        ;;

    start)
        # Extract name of first interface from XML on stdin
        IFACE=$(xpath -n -e '/domain/devices/interface[1]/target/@dev' |& \
            grep -oe 'dev=".*"' | \
            cut -d\" -f2)
        configure_vlans $NAME $IFACE
        ;;

    started)
        ;;

    stopped)
        ;;

    release)
        ;;

esac

) 2>&1 | logger -ei -t ${BASEPROG}
