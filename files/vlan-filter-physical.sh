#!/bin/bash

# Enable vlan filtering on bridges and physical interfaces, enable stats on bridges

# Copyright (c) 2022, Paul D. Gear
# License: AGPL v3 or later

# External dependencies:
# - iproute2
# - jq

set -e
set -u

CONF=/etc/vlan-filters.json

FILTER=$(jq -r ".interfaces.$IFACE.filtering" < ${CONF} 2>/dev/null | grep -ve "^null$" || true)
TAGGED=$(jq -r ".interfaces.$IFACE.tagged[]" < ${CONF} 2>/dev/null | grep -ve "^null$" || true)
UNTAGGED=$(jq -r ".interfaces.$IFACE.untagged" < ${CONF} 2>/dev/null | grep -ve "^null"$ || true)

echo FILTER=$FILTER
echo IFACE=$IFACE
echo TAGGED=$TAGGED
echo UNTAGGED=$UNTAGGED

SELF=""
if [ "$FILTER" = true ]; then
    ip link set $IFACE type bridge vlan_filtering 1
    ip link set $IFACE type bridge vlan_stats 1
    SELF="self"
fi

if [ -n "$UNTAGGED" ]; then
    # clear default VLAN
    if [ -z "$SELF" ]; then
        bridge vlan del dev $IFACE vid 1
    fi

    # add untagged VLAN
    bridge vlan add dev $IFACE vid $UNTAGGED pvid untagged $SELF
fi

if [ -n "$TAGGED" ]; then
    # add tagged VLANs
    for vlan in $TAGGED; do
        bridge vlan add dev $IFACE vid $vlan $SELF
    done
fi

# show results
bridge vlan list
